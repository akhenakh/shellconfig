" Number of spaces that a pre-existing tab is equal to.
" For the amount of space used for a new tab use shiftwidth.
au BufRead,BufNewFile *py,*pyw,*.c,*.h,*html,*js set tabstop=8

" What to use for an indent.
" This will affect Ctrl-T and 'autoindent'.
" Python: 4 spaces
" C: tabs (pre-existing files) or 4 spaces (new files)
au BufRead,BufNewFile *.py,*pyw,*.html,*.js set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw,*.html,*.js set expandtab
fu Select_c_style()
    if search('^\t', 'n', 150)
        set shiftwidth=8
        set noexpandtab
    el 
        set shiftwidth=4
        set expandtab
    en
endf
au BufRead,BufNewFile *.c,*.h call Select_c_style()

au BufRead,BufNewFile Makefile* set noexpandtab

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
au BufRead,BufNewFile *.py,*.pyw set softtabstop=4
au BufNewFile,BufRead *.m,*.h set ft=objc

" Turn off settings in 'formatoptions' relating to comment formatting.
" - c : do not automatically insert the comment leader when wrapping based on
"    'textwidth'
" - o : do not insert the comment leader when using 'o' or 'O' from command mode
" - r : do not insert the comment leader when hitting <Enter> in insert mode
" Python: not needed
" C: prevents insertion of '*' at the beginning of every line in a comment
au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r

" Use UNIX (\n) line endings.
" Only used for new files so as to not force existing files to change their
" line endings.
" Python: yes
" C: yes
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

set nocompatible                " choose no compatibility with legacy vi
" For full syntax highlighting:
let python_highlight_all=1
syntax on
set encoding=utf-8
set showcmd                     " display incomplete commands
filetype plugin indent on       " load file type plugins + indentation
set ruler                           " show line and column number

"" Whitespace
set nowrap                      " don't wrap lines
set tabstop=2 shiftwidth=2      " a tab is two spaces (or set this to 4)
set expandtab                   " use spaces, not tabs (optional)
set backspace=indent,eol,start  " backspace through everything in insert mode

"" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter

" Automatically indent based on file type: 
filetype indent on
filetype plugin on
" " Keep indentation level from previous line: 
set autoindent

" remember position in files with .viminfo
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

" 256 colors
set t_Co=256

" you definitly should install pathogen to manage your scripts
" mkdir -p ~/.vim/autoload ~/.vim/bundle 
" curl -so ~/.vim/autoload/pathogen.vim https://raw.github.com/tpope/vim-pathogen/HEAD/autoload/pathogen.vim
call pathogen#infect()

" you should use ctrlp to open files, cd .vim/bundle; git clone https://github.com/kien/ctrlp.vim.git
" invoke via Ctrl p, \b Ctrl f to switch between buffers, mru, files
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*.so     " Linux/MacOSX

nmap <leader>b :CtrlPBuffer<CR>
" go back to the last edited lines : ''

" open vi with 2 files on the cmd line then switch with :bn (buffer next) :bp (buffer previous) :bd (buffer delete)

" ^w s, split horizontally , ^w v vertically, ^w o, unsplit all, ^w q close current split, 
" ^w t, go back to the left top, ^w w
"
" ^w s = partage l'écran en deux horizontalement
" ^w v = partage l'écran en deux verticalement
"
" Se déplacer dans les splits :
" ^w j = sélectionne le split d'en bas
" ^w k = sélectionne le split d'en haut
" ^w + = agrandit le split actif d'une ligne
" ^w - = réduit le split actif d'une ligne
"
" Et dans les vsplits :
" ^w h = sélectionne le vsplit de gauche
" ^w l = sélectionne le vsplit de droite
" ^w > = agrandit le vsplit actif d'une colonne
" ^w < = réduit le vsplit actif d'une colonne
 
" D delete to the end of the line
"
"" visual line number
" set number

" mouse support: this can be annoying, use shift for select then copy with your term use alt on mac
" prefer the command v or V for whole line, move the cursor, y for copy or d for cut, then p for paste
" set mouse=a 

" toggle paste for paste no indent 
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" solarized scheme
"  $ cd ~/.vim/bundle
"  $ git clone git://github.com/altercation/vim-colors-solarized.git
"  mv vim-colors-solarized ~/.vim/bundle/
set background=dark
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
colorscheme solarized
"colorscheme elflord
"hi Comment ctermfg=darkcyan
"hi Constant ctermbg=cyan

" Python completion should work as is in recent vim, to check for it vim --version | grep python
" then use Ctrl x Ctrl o after obj.

" TextMate like escape completion for known words simply use Ctrl p in insert mode

" For virtualenv support with python put this file in your virtualenv/.vimrc
" Remove the django stuff if not needed
"
" py << EOF
" import os.path
" import sys
" import vim
"
" # SET THIS MANUALLY
" # =================
" DJANGO_SETTINGS_MODULE='foo.settings'
"
" project_base_dir = os.environ['VIRTUAL_ENV']
"
" sys.path.insert(0, project_base_dir)
"
" activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
" execfile(activate_this, dict(__file__=activate_this))
"
" os.environ['DJANGO_SETTINGS_MODULE'] = DJANGO_SETTINGS_MODULE
" EOF
if filereadable($VIRTUAL_ENV . '/.vimrc')
  source $VIRTUAL_ENV/.vimrc
endif

" If you prefer the Omni-Completion tip window to close when a selection is
" made, these lines close it on movement in insert mode or when leaving
" insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Auto gofmt gocode
 autocmd FileType go autocmd BufWritePre <buffer> Fmt

" install tagbar go get -u github.com/jstemmer/gotags +
" http://majutsushi.github.io/tagbar/
nmap <F3> :TagbarToggle<CR>
" go tagbar support
let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
\ }
